import { test } from '@japa/runner'
import Institution from 'App/Models/Institution'

test('institutions list - "{label}"')
  .with([
    { label: 'no-language', header: '', expectedNameProp: 'nameEn' },
    {
      label: 'english us then spanish',
      header: 'en-US, en;q=0.9, es;q=0.8',
      expectedNameProp: 'nameEn',
    },
    {
      label: 'english gb then spanish',
      header: 'en-GB, en;q=0.9, es;q=0.8',
      expectedNameProp: 'nameEn',
    },
    {
      label: 'spanish ec then english',
      header: 'es-EC, es;q=0.9, en;q=0.8',
      expectedNameProp: 'nameEs',
    },
    {
      label: 'spanish es then english',
      header: 'es-ES, es;q=0.9, en;q=0.8',
      expectedNameProp: 'nameEs',
    },
    {
      label: 'spanish mx then english',
      header: 'es-MX, es;q=0.9, en;q=0.8',
      expectedNameProp: 'nameEs',
    },
    {
      label: 'french then english',
      header: 'fr-CH, fr;q=0.9, en;q=0.8',
      expectedNameProp: 'nameEn',
    },
  ])
  .run(async ({ client }, row) => {
    let [institution1, institution2] = await Institution.all()

    const response = await client.get('/institution').header('Accept-Language', row.header)

    response.assertStatus(200)
    response.assertBodyContains([
      { institutionId: institution1.id, institutionName: institution1[row.expectedNameProp] },
      { institutionId: institution2.id, institutionName: institution2[row.expectedNameProp] },
    ])
  })
