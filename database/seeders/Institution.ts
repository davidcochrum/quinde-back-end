import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Institution from 'App/Models/Institution'

export default class extends BaseSeeder {
  public async run() {
    await Institution.createMany([
      { nameEn: 'First Institution', nameEs: 'Institución Primero' },
      { nameEn: 'Second Institution', nameEs: 'Institución Segundo' },
    ])
  }
}
