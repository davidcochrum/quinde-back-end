import dotenv from 'dotenv'
dotenv.config()

export default {
  uri: process.env.MONGODB_URI,
  migrationsPath: './migrations',
}
