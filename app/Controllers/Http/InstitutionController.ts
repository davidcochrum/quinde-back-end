import Institution from 'App/Models/Institution'

export default class InstitutionController {
  public async index({ request, response }) {
    const language = request.language(['en', 'es']) ?? 'en'
    const models = await Institution.all()
    return response.ok(
      models.map((model) => {
        return {
          institutionId: model.id,
          institutionName: language === 'es' ? model.nameEs : model.nameEn,
        }
      })
    )
  }
}
