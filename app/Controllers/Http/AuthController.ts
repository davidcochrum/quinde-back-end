import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { rules, schema } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'

export default class AuthController {
  public async register({ request, response }: HttpContextContract) {
    const input = await request.validate({
      schema: schema.create({
        emailAddress: schema.string([rules.email(), rules.maxLength(255)]),
        password: schema.string([rules.minLength(8), rules.maxLength(180)]),
      }),
    })

    const user = await User.create({
      email: input.emailAddress,
      password: input.password,
    })
    response.created(user)
  }

  public async login({ request, response, auth }: HttpContextContract) {
    try {
      return await auth.use('api').attempt(request.input('emailAddress'), request.input('password'))
    } catch (err) {
      return response.unauthorized('Invalid credentials')
    }
  }

  public async show({ auth }: HttpContextContract) {
    return auth.use('api').user
  }

  public async logout({ response, auth }: HttpContextContract) {
    const result = await auth.logout()
    response.gone(result)
  }
}
